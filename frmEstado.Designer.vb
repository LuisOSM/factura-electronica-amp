﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEstado
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEstado))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.lvwOutFiles = New System.Windows.Forms.ListView
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.gbxIn = New System.Windows.Forms.GroupBox
        Me.lvwInFiles = New System.Windows.Forms.ListView
        Me.ColumnHeader4 = New System.Windows.Forms.ColumnHeader
        Me.StatusArea = New System.Windows.Forms.StatusStrip
        Me.tsStatus = New System.Windows.Forms.ToolStripStatusLabel
        Me.GroupBox1.SuspendLayout()
        Me.gbxIn.SuspendLayout()
        Me.StatusArea.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lvwOutFiles)
        Me.GroupBox1.Location = New System.Drawing.Point(315, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(288, 331)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Bandeja de Salida"
        '
        'lvwOutFiles
        '
        Me.lvwOutFiles.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader2})
        Me.lvwOutFiles.GridLines = True
        Me.lvwOutFiles.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvwOutFiles.Location = New System.Drawing.Point(8, 19)
        Me.lvwOutFiles.Name = "lvwOutFiles"
        Me.lvwOutFiles.Size = New System.Drawing.Size(274, 303)
        Me.lvwOutFiles.TabIndex = 1
        Me.lvwOutFiles.UseCompatibleStateImageBehavior = False
        Me.lvwOutFiles.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "                              COMPROBANTE"
        Me.ColumnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ColumnHeader2.Width = 249
        '
        'gbxIn
        '
        Me.gbxIn.Controls.Add(Me.lvwInFiles)
        Me.gbxIn.Location = New System.Drawing.Point(12, 12)
        Me.gbxIn.Name = "gbxIn"
        Me.gbxIn.Size = New System.Drawing.Size(288, 331)
        Me.gbxIn.TabIndex = 3
        Me.gbxIn.TabStop = False
        Me.gbxIn.Text = "Bandeja de Entrada"
        '
        'lvwInFiles
        '
        Me.lvwInFiles.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader4})
        Me.lvwInFiles.GridLines = True
        Me.lvwInFiles.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvwInFiles.Location = New System.Drawing.Point(8, 19)
        Me.lvwInFiles.Name = "lvwInFiles"
        Me.lvwInFiles.Size = New System.Drawing.Size(274, 303)
        Me.lvwInFiles.TabIndex = 1
        Me.lvwInFiles.UseCompatibleStateImageBehavior = False
        Me.lvwInFiles.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "                              COMPROBANTE"
        Me.ColumnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ColumnHeader4.Width = 250
        '
        'StatusArea
        '
        Me.StatusArea.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsStatus})
        Me.StatusArea.Location = New System.Drawing.Point(0, 348)
        Me.StatusArea.Name = "StatusArea"
        Me.StatusArea.Size = New System.Drawing.Size(615, 22)
        Me.StatusArea.SizingGrip = False
        Me.StatusArea.TabIndex = 4
        '
        'tsStatus
        '
        Me.tsStatus.AutoSize = False
        Me.tsStatus.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.tsStatus.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.tsStatus.Name = "tsStatus"
        Me.tsStatus.Size = New System.Drawing.Size(600, 17)
        Me.tsStatus.Spring = True
        Me.tsStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmEstado
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(615, 370)
        Me.Controls.Add(Me.StatusArea)
        Me.Controls.Add(Me.gbxIn)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmEstado"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Visor de estados - Facturación Electrónica (WSFEv1) V.1.0.1 - AMP"
        Me.GroupBox1.ResumeLayout(False)
        Me.gbxIn.ResumeLayout(False)
        Me.StatusArea.ResumeLayout(False)
        Me.StatusArea.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lvwOutFiles As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents gbxIn As System.Windows.Forms.GroupBox
    Friend WithEvents lvwInFiles As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents StatusArea As System.Windows.Forms.StatusStrip
    Friend WithEvents tsStatus As System.Windows.Forms.ToolStripStatusLabel
End Class
