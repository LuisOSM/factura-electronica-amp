Imports Microsoft.VisualBasic
Imports System.net.Mail

Public Class SendMail

    Private m_Host As String = "mail.osam.org.ar"
    Private m_Puerto As Integer = 25
    Private m_User As String = "cae@osam.org.ar"
    Private m_Password As String = "C123ae*"

    Public Sub New(ByVal Host As String, ByVal Puerto As Integer, ByVal User As String, ByVal Password As String)
        m_Host = Host
        m_User = User
        m_Password = Password
    End Sub

    Public Function SendEmailMessage(ByVal strFrom As String, ByVal strTo _
        As String, ByVal strSubject _
        As String, ByVal strMessage _
        As String, ByVal file As String, ByRef outErrorDescription As String) As Boolean
        'This procedure overrides the first procedure and accepts a single
        'string for the recipient and file attachement 
        Try
            Dim MailMsg As New MailMessage(New MailAddress(strFrom.Trim()), New MailAddress(strTo))
            MailMsg.BodyEncoding = System.Text.Encoding.Default
            MailMsg.Subject = strSubject.Trim()
            MailMsg.Body = strMessage.Trim() & vbCrLf
            MailMsg.Priority = MailPriority.High
            MailMsg.IsBodyHtml = True

            If Not file = "" Then
                Dim MsgAttach As New Attachment(file)
                MailMsg.Attachments.Add(MsgAttach)
            End If

            'Smtpclient to send the mail message
            Dim SmtpMail As New SmtpClient(Me.m_Host, Me.m_Host)
            SmtpMail.Credentials = New System.Net.NetworkCredential(Me.m_User, Me.m_Password)
            SmtpMail.Send(MailMsg)
            outErrorDescription = ""
            Return True
        Catch ex As Exception
            outErrorDescription = ex.Message
            Return False
        End Try
    End Function

    Public Function SendEmailMessage(ByVal strFrom As String, ByVal strTo() _
        As String, ByVal strSubject _
        As String, ByVal strMessage _
        As String, ByVal fileList() As String, ByRef outErrorDescription As String) As Boolean
        'This procedure takes string array parameters for multiple recipients and files
        Try
            For Each item As String In strTo
                'For each to address create a mail message
                Dim MailMsg As New MailMessage(New MailAddress(strFrom.Trim()), New MailAddress(item))
                MailMsg.BodyEncoding = System.Text.Encoding.Default
                MailMsg.Subject = strSubject.Trim()
                MailMsg.Body = strMessage.Trim() & vbCrLf
                MailMsg.Priority = MailPriority.High
                MailMsg.IsBodyHtml = True

                'attach each file attachment
                For Each strfile As String In fileList
                    If Not strfile = "" Then
                        Dim MsgAttach As New Attachment(strfile)
                        MailMsg.Attachments.Add(MsgAttach)
                    End If
                Next

                'Smtpclient to send the mail message
                Dim SmtpMail As New SmtpClient(Me.m_Host, Me.m_Host)
                SmtpMail.Credentials = New System.Net.NetworkCredential(Me.m_User, Me.m_Password)
                SmtpMail.Send(MailMsg)
            Next
            outErrorDescription = ""
            Return True
        Catch ex As Exception
            outErrorDescription = ex.Message
            Return False
        End Try
    End Function

    Public Function SendEmailMessage(ByVal strFrom As String, ByVal strTo() _
        As String, ByVal strSubject _
        As String, ByVal strMessage _
        As String, ByRef outErrorDescription As String) As Boolean
        'This procedure takes string array parameters for multiple recipients
        Try
            For Each item As String In strTo
                'For each to address create a mail message
                Dim MailMsg As New MailMessage(New MailAddress(strFrom.Trim()), New MailAddress(item))
                MailMsg.BodyEncoding = System.Text.Encoding.Default
                MailMsg.Subject = strSubject.Trim()
                MailMsg.Body = strMessage.Trim() & vbCrLf
                MailMsg.Priority = MailPriority.High
                MailMsg.IsBodyHtml = True

                'Smtpclient to send the mail message
                Dim SmtpMail As New SmtpClient(Me.m_Host, Me.m_Puerto)
                SmtpMail.Credentials = New System.Net.NetworkCredential(Me.m_User, Me.m_Password)
                SmtpMail.Send(MailMsg)
            Next
            outErrorDescription = ""
            Return True
        Catch ex As Exception
            outErrorDescription = ex.Message
            Return False
        End Try
    End Function

End Class
