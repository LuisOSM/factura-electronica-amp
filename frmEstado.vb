Imports System.Diagnostics
Imports System.Text
Imports System.IO

Public Class frmEstado
    Private WithEvents tmrCrono As Timers.Timer

    Private Sub SetUpTimer()
        Try
            tmrCrono = New Timers.Timer()
            With tmrCrono
                .AutoReset = True
                .Interval = 1000
                .Start()
            End With
        Catch obEx As Exception
            LogToFile("frmEstado.SetupTimer.Error: " & obEx.Message)
        End Try
    End Sub

    Private Sub SetDownTimer()
        If tmrCrono IsNot Nothing Then
            tmrCrono.Stop()
            tmrCrono.Dispose()
        End If
    End Sub

    Public Sub RefreshBandejaDeEntrada()
        'CheckForIllegalCrossThreadCalls = False
        Try
            If mdlIEFEMain.AppConfig.InPath = vbNullString Then
            ElseIf Directory.Exists(mdlIEFEMain.AppConfig.InPath) = True Then
                Dim diiPath As New DirectoryInfo(mdlIEFEMain.AppConfig.InPath)
                Dim fiiList As FileInfo() = diiPath.GetFiles()
                Me.lvwInFiles.Items.Clear()
                For Each fi As FileInfo In fiiList
                    Me.lvwInFiles.Items.Add(fi.Name.ToUpper.Replace(".TXT", ""))
                Next fi
            End If
        Catch ex As Exception
            LogToFile("RefreshBandejaDeEntrada.Error: " & ex.Message)
        End Try
    End Sub

    Public Sub RefreshBandejaDeSalida()
        'CheckForIllegalCrossThreadCalls = False
        Try
            If mdlIEFEMain.AppConfig.InPath = vbNullString Then
            ElseIf Directory.Exists(mdlIEFEMain.AppConfig.OutPath) = True Then
                Dim diiPath As New DirectoryInfo(mdlIEFEMain.AppConfig.OutPath)
                Dim fiiList As FileInfo() = diiPath.GetFiles()
                Me.lvwOutFiles.Items.Clear()
                For Each fi As FileInfo In fiiList
                    Me.lvwOutFiles.Items.Add(fi.Name.ToUpper.Replace(".TXT", ""))
                Next fi
            End If
        Catch ex As Exception
            LogToFile("RefreshBandejaDeSalida.Error: " & ex.Message)
        End Try
    End Sub

    Public Sub tmrCrono_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles tmrCrono.Elapsed
        Try
            If mdlIEFEMain.mobTimer.Enabled Then
                Dim intSeg As Long = DateDiff(DateInterval.Second, Now, mdlIEFEMain.NextTimeout)
                Me.tsStatus.Text = "Reintento programado en " & Format$(Int(intSeg \ 60), "00") & ":" & Format(Int((intSeg / 60 - intSeg \ 60) * 60), "00")
            ElseIf Me.tsStatus.Text <> "" Then
                Me.tsStatus.Text = ""
            End If
        Catch obEx As Exception
            LogToFile("frmEstado.Timer.Error: " & obEx.Message)
        End Try
    End Sub

    Private Sub frmEstado_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Text = "Visor de estados - Facturación Electrónica (WSFEv1) V." & mdlGetVersion() & " - AMP"
        CheckForIllegalCrossThreadCalls = False
        SetUpTimer()
    End Sub

    Private Sub frmEstado_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        SetDownTimer()
    End Sub
End Class