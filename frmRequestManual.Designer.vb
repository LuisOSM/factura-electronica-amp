<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRequestManual
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCancelar = New System.Windows.Forms.Button
        Me.gbxIn = New System.Windows.Forms.GroupBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.txtResult = New System.Windows.Forms.TextBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.btnSend = New System.Windows.Forms.Button
        Me.presta_serv = New System.Windows.Forms.CheckBox
        Me.gbxEMail = New System.Windows.Forms.GroupBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.fecha_venc_pago = New System.Windows.Forms.TextBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.fecha_serv_hasta = New System.Windows.Forms.TextBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.fecha_serv_desde = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.fecha_cbte = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.imp_op_ex = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.impto_liq_rni = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.impto_liq = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.imp_neto = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.imp_tot_conc = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.imp_total = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.cbt_numero = New System.Windows.Forms.TextBox
        Me.punto_vta = New System.Windows.Forms.TextBox
        Me.tipo_cbte = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.nro_doc = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.tipo_doc = New System.Windows.Forms.TextBox
        Me.lblCaption = New System.Windows.Forms.Label
        Me.id = New System.Windows.Forms.TextBox
        Me.Label18 = New System.Windows.Forms.Label
        Me.txtCAE = New System.Windows.Forms.TextBox
        Me.Label19 = New System.Windows.Forms.Label
        Me.txtVTO = New System.Windows.Forms.TextBox
        Me.gbxIn.SuspendLayout()
        Me.gbxEMail.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(501, 476)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 19
        Me.btnCancelar.Text = "&Cerrar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'gbxIn
        '
        Me.gbxIn.Controls.Add(Me.Label19)
        Me.gbxIn.Controls.Add(Me.txtVTO)
        Me.gbxIn.Controls.Add(Me.Label18)
        Me.gbxIn.Controls.Add(Me.txtCAE)
        Me.gbxIn.Controls.Add(Me.Label17)
        Me.gbxIn.Controls.Add(Me.txtResult)
        Me.gbxIn.Controls.Add(Me.Label16)
        Me.gbxIn.Controls.Add(Me.btnSend)
        Me.gbxIn.Controls.Add(Me.presta_serv)
        Me.gbxIn.Controls.Add(Me.gbxEMail)
        Me.gbxIn.Controls.Add(Me.lblCaption)
        Me.gbxIn.Controls.Add(Me.id)
        Me.gbxIn.Location = New System.Drawing.Point(12, 12)
        Me.gbxIn.Name = "gbxIn"
        Me.gbxIn.Size = New System.Drawing.Size(564, 458)
        Me.gbxIn.TabIndex = 7
        Me.gbxIn.TabStop = False
        Me.gbxIn.Text = "Solicitud de CAE"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(87, 320)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(58, 13)
        Me.Label17.TabIndex = 13
        Me.Label17.Text = "Respuesta"
        '
        'txtResult
        '
        Me.txtResult.Location = New System.Drawing.Point(6, 346)
        Me.txtResult.Multiline = True
        Me.txtResult.Name = "txtResult"
        Me.txtResult.Size = New System.Drawing.Size(550, 106)
        Me.txtResult.TabIndex = 18
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(495, 22)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(61, 13)
        Me.Label16.TabIndex = 11
        Me.Label16.Text = "Cantidad: 1"
        '
        'btnSend
        '
        Me.btnSend.Location = New System.Drawing.Point(6, 315)
        Me.btnSend.Name = "btnSend"
        Me.btnSend.Size = New System.Drawing.Size(75, 23)
        Me.btnSend.TabIndex = 17
        Me.btnSend.Text = "Consultar"
        Me.btnSend.UseVisualStyleBackColor = True
        '
        'presta_serv
        '
        Me.presta_serv.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.presta_serv.Location = New System.Drawing.Point(246, 17)
        Me.presta_serv.Name = "presta_serv"
        Me.presta_serv.Size = New System.Drawing.Size(117, 24)
        Me.presta_serv.TabIndex = 1
        Me.presta_serv.Text = "Presta Serv."
        Me.presta_serv.UseVisualStyleBackColor = True
        '
        'gbxEMail
        '
        Me.gbxEMail.Controls.Add(Me.Label15)
        Me.gbxEMail.Controls.Add(Me.fecha_venc_pago)
        Me.gbxEMail.Controls.Add(Me.Label14)
        Me.gbxEMail.Controls.Add(Me.fecha_serv_hasta)
        Me.gbxEMail.Controls.Add(Me.Label13)
        Me.gbxEMail.Controls.Add(Me.fecha_serv_desde)
        Me.gbxEMail.Controls.Add(Me.Label12)
        Me.gbxEMail.Controls.Add(Me.fecha_cbte)
        Me.gbxEMail.Controls.Add(Me.Label9)
        Me.gbxEMail.Controls.Add(Me.imp_op_ex)
        Me.gbxEMail.Controls.Add(Me.Label1)
        Me.gbxEMail.Controls.Add(Me.impto_liq_rni)
        Me.gbxEMail.Controls.Add(Me.Label8)
        Me.gbxEMail.Controls.Add(Me.impto_liq)
        Me.gbxEMail.Controls.Add(Me.Label4)
        Me.gbxEMail.Controls.Add(Me.imp_neto)
        Me.gbxEMail.Controls.Add(Me.Label3)
        Me.gbxEMail.Controls.Add(Me.imp_tot_conc)
        Me.gbxEMail.Controls.Add(Me.Label2)
        Me.gbxEMail.Controls.Add(Me.imp_total)
        Me.gbxEMail.Controls.Add(Me.Label5)
        Me.gbxEMail.Controls.Add(Me.Label10)
        Me.gbxEMail.Controls.Add(Me.Label11)
        Me.gbxEMail.Controls.Add(Me.cbt_numero)
        Me.gbxEMail.Controls.Add(Me.punto_vta)
        Me.gbxEMail.Controls.Add(Me.tipo_cbte)
        Me.gbxEMail.Controls.Add(Me.Label6)
        Me.gbxEMail.Controls.Add(Me.nro_doc)
        Me.gbxEMail.Controls.Add(Me.Label7)
        Me.gbxEMail.Controls.Add(Me.tipo_doc)
        Me.gbxEMail.Location = New System.Drawing.Point(6, 47)
        Me.gbxEMail.Name = "gbxEMail"
        Me.gbxEMail.Size = New System.Drawing.Size(551, 262)
        Me.gbxEMail.TabIndex = 8
        Me.gbxEMail.TabStop = False
        Me.gbxEMail.Text = "Detalle"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(24, 156)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(96, 13)
        Me.Label15.TabIndex = 25
        Me.Label15.Text = "Fecha Venc. Pago"
        '
        'fecha_venc_pago
        '
        Me.fecha_venc_pago.Location = New System.Drawing.Point(124, 149)
        Me.fecha_venc_pago.Name = "fecha_venc_pago"
        Me.fecha_venc_pago.Size = New System.Drawing.Size(83, 20)
        Me.fecha_venc_pago.TabIndex = 10
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(24, 130)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(93, 13)
        Me.Label14.TabIndex = 23
        Me.Label14.Text = "Fecha Svc. Hasta"
        '
        'fecha_serv_hasta
        '
        Me.fecha_serv_hasta.Location = New System.Drawing.Point(124, 123)
        Me.fecha_serv_hasta.Name = "fecha_serv_hasta"
        Me.fecha_serv_hasta.Size = New System.Drawing.Size(83, 20)
        Me.fecha_serv_hasta.TabIndex = 9
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(24, 104)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(96, 13)
        Me.Label13.TabIndex = 21
        Me.Label13.Text = "Fecha Svc. Desde"
        '
        'fecha_serv_desde
        '
        Me.fecha_serv_desde.Location = New System.Drawing.Point(124, 97)
        Me.fecha_serv_desde.Name = "fecha_serv_desde"
        Me.fecha_serv_desde.Size = New System.Drawing.Size(83, 20)
        Me.fecha_serv_desde.TabIndex = 8
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(24, 78)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(76, 13)
        Me.Label12.TabIndex = 19
        Me.Label12.Text = "Fecha Emisión"
        '
        'fecha_cbte
        '
        Me.fecha_cbte.Location = New System.Drawing.Point(124, 71)
        Me.fecha_cbte.Name = "fecha_cbte"
        Me.fecha_cbte.Size = New System.Drawing.Size(83, 20)
        Me.fecha_cbte.TabIndex = 7
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(242, 156)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(49, 13)
        Me.Label9.TabIndex = 17
        Me.Label9.Text = "Exento $"
        '
        'imp_op_ex
        '
        Me.imp_op_ex.Location = New System.Drawing.Point(342, 149)
        Me.imp_op_ex.Name = "imp_op_ex"
        Me.imp_op_ex.Size = New System.Drawing.Size(100, 20)
        Me.imp_op_ex.TabIndex = 13
        Me.imp_op_ex.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(242, 208)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(93, 13)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "IVA No Inscripto $"
        '
        'impto_liq_rni
        '
        Me.impto_liq_rni.Location = New System.Drawing.Point(342, 201)
        Me.impto_liq_rni.Name = "impto_liq_rni"
        Me.impto_liq_rni.Size = New System.Drawing.Size(100, 20)
        Me.impto_liq_rni.TabIndex = 15
        Me.impto_liq_rni.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(242, 182)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(33, 13)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "IVA $"
        '
        'impto_liq
        '
        Me.impto_liq.Location = New System.Drawing.Point(342, 175)
        Me.impto_liq.Name = "impto_liq"
        Me.impto_liq.Size = New System.Drawing.Size(100, 20)
        Me.impto_liq.TabIndex = 14
        Me.impto_liq.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(242, 104)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(39, 13)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Neto $"
        '
        'imp_neto
        '
        Me.imp_neto.Location = New System.Drawing.Point(342, 97)
        Me.imp_neto.Name = "imp_neto"
        Me.imp_neto.Size = New System.Drawing.Size(100, 20)
        Me.imp_neto.TabIndex = 11
        Me.imp_neto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(242, 130)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(74, 13)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "No Gravado $"
        '
        'imp_tot_conc
        '
        Me.imp_tot_conc.Location = New System.Drawing.Point(342, 123)
        Me.imp_tot_conc.Name = "imp_tot_conc"
        Me.imp_tot_conc.Size = New System.Drawing.Size(100, 20)
        Me.imp_tot_conc.TabIndex = 12
        Me.imp_tot_conc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(242, 234)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(40, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Total $"
        '
        'imp_total
        '
        Me.imp_total.Location = New System.Drawing.Point(342, 227)
        Me.imp_total.Name = "imp_total"
        Me.imp_total.Size = New System.Drawing.Size(100, 20)
        Me.imp_total.TabIndex = 16
        Me.imp_total.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(19, 52)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(94, 13)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Tipo Comprobante"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(387, 52)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(44, 13)
        Me.Label10.TabIndex = 3
        Me.Label10.Text = "Número"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(237, 52)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(81, 13)
        Me.Label11.TabIndex = 1
        Me.Label11.Text = "Punto de Venta"
        '
        'cbt_numero
        '
        Me.cbt_numero.Location = New System.Drawing.Point(437, 45)
        Me.cbt_numero.Name = "cbt_numero"
        Me.cbt_numero.Size = New System.Drawing.Size(74, 20)
        Me.cbt_numero.TabIndex = 6
        '
        'punto_vta
        '
        Me.punto_vta.Location = New System.Drawing.Point(342, 45)
        Me.punto_vta.Name = "punto_vta"
        Me.punto_vta.Size = New System.Drawing.Size(34, 20)
        Me.punto_vta.TabIndex = 5
        '
        'tipo_cbte
        '
        Me.tipo_cbte.Location = New System.Drawing.Point(124, 45)
        Me.tipo_cbte.Name = "tipo_cbte"
        Me.tipo_cbte.Size = New System.Drawing.Size(34, 20)
        Me.tipo_cbte.TabIndex = 4
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(237, 26)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(85, 13)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "Nro. Documento"
        '
        'nro_doc
        '
        Me.nro_doc.Location = New System.Drawing.Point(342, 19)
        Me.nro_doc.Name = "nro_doc"
        Me.nro_doc.Size = New System.Drawing.Size(169, 20)
        Me.nro_doc.TabIndex = 3
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(19, 26)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(86, 13)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "Tipo Documento"
        '
        'tipo_doc
        '
        Me.tipo_doc.Location = New System.Drawing.Point(124, 19)
        Me.tipo_doc.Name = "tipo_doc"
        Me.tipo_doc.Size = New System.Drawing.Size(34, 20)
        Me.tipo_doc.TabIndex = 2
        '
        'lblCaption
        '
        Me.lblCaption.AutoSize = True
        Me.lblCaption.Location = New System.Drawing.Point(25, 26)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(21, 13)
        Me.lblCaption.TabIndex = 1
        Me.lblCaption.Text = " ID"
        '
        'id
        '
        Me.id.Location = New System.Drawing.Point(130, 19)
        Me.id.Name = "id"
        Me.id.Size = New System.Drawing.Size(56, 20)
        Me.id.TabIndex = 0
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(180, 321)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(28, 13)
        Me.Label18.TabIndex = 27
        Me.Label18.Text = "CAE"
        '
        'txtCAE
        '
        Me.txtCAE.Location = New System.Drawing.Point(214, 317)
        Me.txtCAE.Name = "txtCAE"
        Me.txtCAE.Size = New System.Drawing.Size(83, 20)
        Me.txtCAE.TabIndex = 26
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(393, 320)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(65, 13)
        Me.Label19.TabIndex = 29
        Me.Label19.Text = "Vencimiento"
        '
        'txtVTO
        '
        Me.txtVTO.Location = New System.Drawing.Point(473, 317)
        Me.txtVTO.Name = "txtVTO"
        Me.txtVTO.Size = New System.Drawing.Size(83, 20)
        Me.txtVTO.TabIndex = 28
        '
        'frmRequestManual
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(587, 504)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.gbxIn)
        Me.Name = "frmRequestManual"
        Me.Text = "frmRequestManual"
        Me.gbxIn.ResumeLayout(False)
        Me.gbxIn.PerformLayout()
        Me.gbxEMail.ResumeLayout(False)
        Me.gbxEMail.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents gbxIn As System.Windows.Forms.GroupBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents cbt_numero As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents punto_vta As System.Windows.Forms.TextBox
    Friend WithEvents gbxEMail As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents tipo_cbte As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents nro_doc As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents tipo_doc As System.Windows.Forms.TextBox
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents id As System.Windows.Forms.TextBox
    Friend WithEvents presta_serv As System.Windows.Forms.CheckBox
    Friend WithEvents btnSend As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents impto_liq As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents imp_neto As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents imp_tot_conc As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents imp_total As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents fecha_cbte As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents imp_op_ex As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents impto_liq_rni As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents fecha_venc_pago As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents fecha_serv_hasta As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents fecha_serv_desde As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtResult As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtVTO As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtCAE As System.Windows.Forms.TextBox
End Class
