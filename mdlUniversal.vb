Module mdlUniversal

    Private MsgText As String
    Private FileWithError As String

    Public Sub LogToFile(ByVal Texto As String)
        Try
            Dim ff As System.IO.StreamWriter = Microsoft.VisualBasic.FileIO.FileSystem.OpenTextFileWriter(My.Application.Info.DirectoryPath & "\Log.txt", True, System.Text.Encoding.ASCII)
            ff.WriteLine(Texto)
            ff.Close()
            'Console.WriteLine(Texto)
        Catch
        End Try
    End Sub

    Public Sub LogToFileAndScreen(ByVal Texto As String, ByVal ArchivoAfectado As String)
        Try
            LogToFile(Texto)
            'mobNotifyIcon.ShowBalloonTip(5000, "ERROR", Texto, ToolTipIcon.Info)
            'MsgBox(Texto, MsgBoxStyle.Exclamation, "ERROR")
            MsgText = Texto
            FileWithError = ArchivoAfectado
            Dim NT As New System.Threading.Thread(AddressOf NonModalMessageBox)
            NT.Start()
            NT = Nothing
        Catch
        End Try
    End Sub

    Private Sub NonModalMessageBox()
        Try
            Dim email As New SendMail(AppConfig.Host, AppConfig.Puerto, AppConfig.User, AppConfig.Password)
            Dim ToList() As String = AppConfig.NotifyTo.Split(";")
            Dim strErrorDescription As String = ""
            If InData.FileName Is Nothing Then InData.FileName = ""
            email.SendEmailMessage("cae@osam.org.ar", ToList, "AMP - ERROR CAE " & FileWithError & "", MsgText, strErrorDescription)
            If strErrorDescription <> "" Then
                LogToFile("NonModalMessageBox.email.Error: " & strErrorDescription)
            End If
        Catch ex As Exception
            LogToFile("NonModalMessageBox.sendmail.Error: " & ex.Message)
        End Try
        MessageBox.Show(MsgText, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    End Sub

    Public Function mdlGetVersion() As String
        '1.0.3 2014/12/03: manejo de error 600
        '1.0.4 2014/12/09: mdlWSFEv1.ResetToken() era el motivo del err.600
        '1.0.5 2015/08/11: bugfix Today
        '1.0.6 2018/03/06: 
        '1.0.7 2019/03/30: campos extra para percepcion iibb
        '1.0.8 2021/04/11: Res. Circulacion abierta y comprobante asociado
        mdlGetVersion = "1.0.8"
    End Function

End Module
