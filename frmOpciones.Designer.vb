<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOpciones
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOpciones))
        Me.gbxIn = New System.Windows.Forms.GroupBox
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.txtQueryCAE_Numero = New System.Windows.Forms.TextBox
        Me.btnConsultaCAE = New System.Windows.Forms.Button
        Me.Label15 = New System.Windows.Forms.Label
        Me.txtQueryCAE_TipoCom = New System.Windows.Forms.TextBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.txtQueryCAE_Pdv = New System.Windows.Forms.TextBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.txtTmpPath = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.txtErrPath = New System.Windows.Forms.TextBox
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.txtRetry = New System.Windows.Forms.TextBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.txtDifFecha = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.btnFecha = New System.Windows.Forms.Button
        Me.Label9 = New System.Windows.Forms.Label
        Me.txtFecha = New System.Windows.Forms.TextBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.btnConsultar = New System.Windows.Forms.Button
        Me.Label10 = New System.Windows.Forms.Label
        Me.txtCOM = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.txtPDV = New System.Windows.Forms.TextBox
        Me.gbxEMail = New System.Windows.Forms.GroupBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtPassword = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtUser = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtPuerto = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtHost = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtNotifyTo = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtHistorytPath = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtOutPath = New System.Windows.Forms.TextBox
        Me.lblCaption = New System.Windows.Forms.Label
        Me.txtInFolder = New System.Windows.Forms.TextBox
        Me.btnAceptar = New System.Windows.Forms.Button
        Me.btnCancelar = New System.Windows.Forms.Button
        Me.btnDummy = New System.Windows.Forms.Button
        Me.btnEmail = New System.Windows.Forms.Button
        Me.gbxIn.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.gbxEMail.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxIn
        '
        Me.gbxIn.Controls.Add(Me.GroupBox4)
        Me.gbxIn.Controls.Add(Me.Label14)
        Me.gbxIn.Controls.Add(Me.txtTmpPath)
        Me.gbxIn.Controls.Add(Me.Label12)
        Me.gbxIn.Controls.Add(Me.txtErrPath)
        Me.gbxIn.Controls.Add(Me.GroupBox3)
        Me.gbxIn.Controls.Add(Me.GroupBox2)
        Me.gbxIn.Controls.Add(Me.GroupBox1)
        Me.gbxIn.Controls.Add(Me.gbxEMail)
        Me.gbxIn.Controls.Add(Me.Label3)
        Me.gbxIn.Controls.Add(Me.txtNotifyTo)
        Me.gbxIn.Controls.Add(Me.Label2)
        Me.gbxIn.Controls.Add(Me.txtHistorytPath)
        Me.gbxIn.Controls.Add(Me.Label1)
        Me.gbxIn.Controls.Add(Me.txtOutPath)
        Me.gbxIn.Controls.Add(Me.lblCaption)
        Me.gbxIn.Controls.Add(Me.txtInFolder)
        Me.gbxIn.Location = New System.Drawing.Point(12, 12)
        Me.gbxIn.Name = "gbxIn"
        Me.gbxIn.Size = New System.Drawing.Size(589, 395)
        Me.gbxIn.TabIndex = 4
        Me.gbxIn.TabStop = False
        Me.gbxIn.Text = "Configuración"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.txtQueryCAE_Numero)
        Me.GroupBox4.Controls.Add(Me.btnConsultaCAE)
        Me.GroupBox4.Controls.Add(Me.Label15)
        Me.GroupBox4.Controls.Add(Me.txtQueryCAE_TipoCom)
        Me.GroupBox4.Controls.Add(Me.Label16)
        Me.GroupBox4.Controls.Add(Me.txtQueryCAE_Pdv)
        Me.GroupBox4.Location = New System.Drawing.Point(273, 313)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(300, 76)
        Me.GroupBox4.TabIndex = 19
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Consulta de Comprobantes"
        '
        'txtQueryCAE_Numero
        '
        Me.txtQueryCAE_Numero.Location = New System.Drawing.Point(126, 49)
        Me.txtQueryCAE_Numero.Name = "txtQueryCAE_Numero"
        Me.txtQueryCAE_Numero.Size = New System.Drawing.Size(68, 20)
        Me.txtQueryCAE_Numero.TabIndex = 30
        '
        'btnConsultaCAE
        '
        Me.btnConsultaCAE.Location = New System.Drawing.Point(219, 47)
        Me.btnConsultaCAE.Name = "btnConsultaCAE"
        Me.btnConsultaCAE.Size = New System.Drawing.Size(75, 23)
        Me.btnConsultaCAE.TabIndex = 31
        Me.btnConsultaCAE.Text = "Consultar"
        Me.btnConsultaCAE.UseVisualStyleBackColor = True
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(19, 30)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(61, 13)
        Me.Label15.TabIndex = 26
        Me.Label15.Text = "Tipo Comp."
        '
        'txtQueryCAE_TipoCom
        '
        Me.txtQueryCAE_TipoCom.Location = New System.Drawing.Point(86, 23)
        Me.txtQueryCAE_TipoCom.Name = "txtQueryCAE_TipoCom"
        Me.txtQueryCAE_TipoCom.Size = New System.Drawing.Size(34, 20)
        Me.txtQueryCAE_TipoCom.TabIndex = 27
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(19, 56)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(44, 13)
        Me.Label16.TabIndex = 28
        Me.Label16.Text = "Número"
        '
        'txtQueryCAE_Pdv
        '
        Me.txtQueryCAE_Pdv.Location = New System.Drawing.Point(86, 49)
        Me.txtQueryCAE_Pdv.Name = "txtQueryCAE_Pdv"
        Me.txtQueryCAE_Pdv.Size = New System.Drawing.Size(34, 20)
        Me.txtQueryCAE_Pdv.TabIndex = 29
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(19, 104)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(91, 13)
        Me.Label14.TabIndex = 16
        Me.Label14.Text = "Carpeta Temporal"
        '
        'txtTmpPath
        '
        Me.txtTmpPath.Location = New System.Drawing.Point(123, 97)
        Me.txtTmpPath.Name = "txtTmpPath"
        Me.txtTmpPath.Size = New System.Drawing.Size(449, 20)
        Me.txtTmpPath.TabIndex = 17
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(20, 78)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(95, 13)
        Me.Label12.TabIndex = 14
        Me.Label12.Text = "Carpeta de Errores"
        '
        'txtErrPath
        '
        Me.txtErrPath.Location = New System.Drawing.Point(124, 71)
        Me.txtErrPath.Name = "txtErrPath"
        Me.txtErrPath.Size = New System.Drawing.Size(449, 20)
        Me.txtErrPath.TabIndex = 15
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label13)
        Me.GroupBox3.Controls.Add(Me.txtRetry)
        Me.GroupBox3.Location = New System.Drawing.Point(350, 254)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(223, 50)
        Me.GroupBox3.TabIndex = 13
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "En error reintentar cada"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(71, 26)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(55, 13)
        Me.Label13.TabIndex = 27
        Me.Label13.Text = "Segundos"
        '
        'txtRetry
        '
        Me.txtRetry.Location = New System.Drawing.Point(21, 19)
        Me.txtRetry.Name = "txtRetry"
        Me.txtRetry.Size = New System.Drawing.Size(44, 20)
        Me.txtRetry.TabIndex = 26
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtDifFecha)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.btnFecha)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.txtFecha)
        Me.GroupBox2.Location = New System.Drawing.Point(350, 172)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(223, 76)
        Me.GroupBox2.TabIndex = 12
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Consulta de Fecha y Hora"
        '
        'txtDifFecha
        '
        Me.txtDifFecha.Location = New System.Drawing.Point(75, 47)
        Me.txtDifFecha.Name = "txtDifFecha"
        Me.txtDifFecha.Size = New System.Drawing.Size(56, 20)
        Me.txtDifFecha.TabIndex = 24
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(6, 52)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(55, 13)
        Me.Label8.TabIndex = 23
        Me.Label8.Text = "Diferencia"
        '
        'btnFecha
        '
        Me.btnFecha.Location = New System.Drawing.Point(137, 47)
        Me.btnFecha.Name = "btnFecha"
        Me.btnFecha.Size = New System.Drawing.Size(75, 23)
        Me.btnFecha.TabIndex = 25
        Me.btnFecha.Text = "Consultar"
        Me.btnFecha.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(6, 26)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(63, 13)
        Me.Label9.TabIndex = 21
        Me.Label9.Text = "Fecha AFIP"
        '
        'txtFecha
        '
        Me.txtFecha.Location = New System.Drawing.Point(75, 19)
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.Size = New System.Drawing.Size(137, 20)
        Me.txtFecha.TabIndex = 22
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnConsultar)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.txtCOM)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.txtPDV)
        Me.GroupBox1.Location = New System.Drawing.Point(22, 305)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(250, 76)
        Me.GroupBox1.TabIndex = 9
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Consulta de numeración"
        '
        'btnConsultar
        '
        Me.btnConsultar.Location = New System.Drawing.Point(164, 47)
        Me.btnConsultar.Name = "btnConsultar"
        Me.btnConsultar.Size = New System.Drawing.Size(75, 23)
        Me.btnConsultar.TabIndex = 20
        Me.btnConsultar.Text = "Consultar"
        Me.btnConsultar.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(19, 52)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(70, 13)
        Me.Label10.TabIndex = 18
        Me.Label10.Text = "Comprobante"
        '
        'txtCOM
        '
        Me.txtCOM.Location = New System.Drawing.Point(124, 45)
        Me.txtCOM.Name = "txtCOM"
        Me.txtCOM.Size = New System.Drawing.Size(34, 20)
        Me.txtCOM.TabIndex = 19
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(19, 26)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(81, 13)
        Me.Label11.TabIndex = 16
        Me.Label11.Text = "Punto de Venta"
        '
        'txtPDV
        '
        Me.txtPDV.Location = New System.Drawing.Point(124, 19)
        Me.txtPDV.Name = "txtPDV"
        Me.txtPDV.Size = New System.Drawing.Size(34, 20)
        Me.txtPDV.TabIndex = 17
        '
        'gbxEMail
        '
        Me.gbxEMail.Controls.Add(Me.btnEmail)
        Me.gbxEMail.Controls.Add(Me.Label4)
        Me.gbxEMail.Controls.Add(Me.txtPassword)
        Me.gbxEMail.Controls.Add(Me.Label5)
        Me.gbxEMail.Controls.Add(Me.txtUser)
        Me.gbxEMail.Controls.Add(Me.Label6)
        Me.gbxEMail.Controls.Add(Me.txtPuerto)
        Me.gbxEMail.Controls.Add(Me.Label7)
        Me.gbxEMail.Controls.Add(Me.txtHost)
        Me.gbxEMail.Location = New System.Drawing.Point(22, 172)
        Me.gbxEMail.Name = "gbxEMail"
        Me.gbxEMail.Size = New System.Drawing.Size(322, 127)
        Me.gbxEMail.TabIndex = 8
        Me.gbxEMail.TabStop = False
        Me.gbxEMail.Text = "Cuenta de correo electrónico para notificación"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(19, 104)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(61, 13)
        Me.Label4.TabIndex = 14
        Me.Label4.Text = "Contraseña"
        '
        'txtPassword
        '
        Me.txtPassword.Location = New System.Drawing.Point(124, 97)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword.Size = New System.Drawing.Size(134, 20)
        Me.txtPassword.TabIndex = 15
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(19, 78)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(43, 13)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Usuario"
        '
        'txtUser
        '
        Me.txtUser.Location = New System.Drawing.Point(124, 71)
        Me.txtUser.Name = "txtUser"
        Me.txtUser.Size = New System.Drawing.Size(192, 20)
        Me.txtUser.TabIndex = 13
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(19, 52)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(38, 13)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Puerto"
        '
        'txtPuerto
        '
        Me.txtPuerto.Location = New System.Drawing.Point(124, 45)
        Me.txtPuerto.Name = "txtPuerto"
        Me.txtPuerto.Size = New System.Drawing.Size(34, 20)
        Me.txtPuerto.TabIndex = 11
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(19, 26)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(46, 13)
        Me.Label7.TabIndex = 8
        Me.Label7.Text = "Servidor"
        '
        'txtHost
        '
        Me.txtHost.Location = New System.Drawing.Point(124, 19)
        Me.txtHost.Name = "txtHost"
        Me.txtHost.Size = New System.Drawing.Size(192, 20)
        Me.txtHost.TabIndex = 9
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(19, 153)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(93, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "En error notificar a"
        '
        'txtNotifyTo
        '
        Me.txtNotifyTo.Location = New System.Drawing.Point(124, 146)
        Me.txtNotifyTo.Name = "txtNotifyTo"
        Me.txtNotifyTo.Size = New System.Drawing.Size(449, 20)
        Me.txtNotifyTo.TabIndex = 7
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(19, 130)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(84, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Carpeta Historial"
        '
        'txtHistorytPath
        '
        Me.txtHistorytPath.Location = New System.Drawing.Point(124, 123)
        Me.txtHistorytPath.Name = "txtHistorytPath"
        Me.txtHistorytPath.Size = New System.Drawing.Size(449, 20)
        Me.txtHistorytPath.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(19, 52)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(91, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Carpeta de Salida"
        '
        'txtOutPath
        '
        Me.txtOutPath.Location = New System.Drawing.Point(123, 45)
        Me.txtOutPath.Name = "txtOutPath"
        Me.txtOutPath.Size = New System.Drawing.Size(449, 20)
        Me.txtOutPath.TabIndex = 3
        '
        'lblCaption
        '
        Me.lblCaption.AutoSize = True
        Me.lblCaption.Location = New System.Drawing.Point(19, 26)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(99, 13)
        Me.lblCaption.TabIndex = 0
        Me.lblCaption.Text = "Carpeta de Entrada"
        '
        'txtInFolder
        '
        Me.txtInFolder.Location = New System.Drawing.Point(124, 19)
        Me.txtInFolder.Name = "txtInFolder"
        Me.txtInFolder.Size = New System.Drawing.Size(449, 20)
        Me.txtInFolder.TabIndex = 1
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(445, 413)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 28
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(526, 413)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 29
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnDummy
        '
        Me.btnDummy.Location = New System.Drawing.Point(12, 413)
        Me.btnDummy.Name = "btnDummy"
        Me.btnDummy.Size = New System.Drawing.Size(75, 23)
        Me.btnDummy.TabIndex = 30
        Me.btnDummy.Text = "Dummy"
        Me.btnDummy.UseVisualStyleBackColor = True
        '
        'btnEmail
        '
        Me.btnEmail.Location = New System.Drawing.Point(264, 97)
        Me.btnEmail.Name = "btnEmail"
        Me.btnEmail.Size = New System.Drawing.Size(52, 23)
        Me.btnEmail.TabIndex = 22
        Me.btnEmail.Text = "Probar"
        Me.btnEmail.UseVisualStyleBackColor = True
        '
        'frmOpciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(613, 443)
        Me.Controls.Add(Me.btnDummy)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.gbxIn)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmOpciones"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Opciones - Facturación Electrónica"
        Me.gbxIn.ResumeLayout(False)
        Me.gbxIn.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.gbxEMail.ResumeLayout(False)
        Me.gbxEMail.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxIn As System.Windows.Forms.GroupBox
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents txtInFolder As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtOutPath As System.Windows.Forms.TextBox
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtHistorytPath As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtNotifyTo As System.Windows.Forms.TextBox
    Friend WithEvents gbxEMail As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtUser As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtPuerto As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtHost As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnConsultar As System.Windows.Forms.Button
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtCOM As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtPDV As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnFecha As System.Windows.Forms.Button
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtFecha As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtDifFecha As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtRetry As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtTmpPath As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtErrPath As System.Windows.Forms.TextBox
    Friend WithEvents btnDummy As System.Windows.Forms.Button
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents txtQueryCAE_Numero As System.Windows.Forms.TextBox
    Friend WithEvents btnConsultaCAE As System.Windows.Forms.Button
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtQueryCAE_TipoCom As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtQueryCAE_Pdv As System.Windows.Forms.TextBox
    Friend WithEvents btnEmail As System.Windows.Forms.Button
End Class
